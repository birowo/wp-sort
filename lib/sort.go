package sort

import "runtime"

var workersNum = runtime.NumCPU()

func partition(start, end int, less func(int, int) bool, swap func(int, int)) int {
	for i := start; i < end; i++ {
		if less(i, end) {
			swap(i, start)
			start++
		}
	}
	swap(start, end)
	return start
}
func Quick(slcLen int, less func(int, int) bool, swap func(int, int)) {
	type Part struct {
		start, end int
	}
	jobs := make(chan Part, slcLen)
	jobs <- Part{0, slcLen - 1}
	done := make(chan struct{})
	for id := 0; id < workersNum; id++ {
		go func() {
			for len(jobs) != 0 {
				part := <-jobs
				pivot := partition(part.start, part.end, less, swap)
				if pivot-1 > part.start {
					jobs <- Part{part.start, pivot - 1}
				}
				if pivot+1 < part.end {
					jobs <- Part{pivot + 1, part.end}
				}
			}
			done <- struct{}{}
		}()
	}
	<-done
}
