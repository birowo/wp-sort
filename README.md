# wp-sort

#GOLANG
quicksort
worker pools

**install:** `go get -u gitlab.com/birowo/wp-sort`

**quicksort:** [iterative implementation of quicksort](https://www.techiedelight.com/iterative-implementation-of-quicksort/)

**worker pools:** [worker pools](https://gobyexample.com/worker-pools)

[POC](https://play.golang.org/p/EhcEjhjZcKa)